package com.rdc.p2p.adapter;

public interface OnItemViewClickListener {
    void onImageClick(int position);
    void onAlterClick(int position);

}
