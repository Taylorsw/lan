package com.rdc.p2p.adapter;


public interface OnClickRecyclerViewListener {

    void onItemClick(int position);
    boolean onItemLongClick(int position);

}
