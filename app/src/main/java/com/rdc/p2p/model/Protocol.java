package com.rdc.p2p.model;

public interface Protocol {

    int TEXT = 0;
    int IMAGE = 1;
    int CONNECT = 3;
    int CONNECT_RESPONSE = 4;
    int DISCONNECT = 5;
    int KEEP_USER = 6;
    int KEEP_USER_RESPONSE = 9;
}
