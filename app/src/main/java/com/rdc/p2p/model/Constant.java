package com.rdc.p2p.model;


public interface Constant {

    int UPDATE_SEND_MSG_STATE = 5;
    int UPDATE_FILE_STATE = 6;
    int SEND_MSG_ING = 0;
    int SEND_MSG_ERROR = 1;
    int SEND_MSG_FINISH = 2;

}
