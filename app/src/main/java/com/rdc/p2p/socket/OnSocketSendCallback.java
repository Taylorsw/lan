package com.rdc.p2p.socket;

import com.rdc.p2p.model.MessageBean;

public interface OnSocketSendCallback {

    /**
     * 发送消息成功(图片、文字)
     */
    void sendMsgSuccess(int position);

    /**
     * 发送消息失败
     */
    void sendMsgError(int position);

    /**
     * 发送文件中...
     */
    void fileSending(int position,MessageBean messageBean);
}
