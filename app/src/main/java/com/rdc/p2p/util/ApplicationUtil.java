package com.rdc.p2p.util;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.rdc.p2p.model.UserBean;

import org.litepal.LitePal;


public class ApplicationUtil extends Application {
    
    @SuppressLint("StaticFieldLeak")
    private static Context sContxet;
    private static UserBean sUserBean;
    private static String sMyIP;

    @Override
    public void onCreate() {
        super.onCreate();
        LitePal.initialize(this);
        sContxet = getApplicationContext();
        sUserBean = getUserBean();
        sMyIP = getMyIP();
    }

    public static String getMyIP() {
        if (sMyIP == null){
            sMyIP = UserUtil.getMyIp();
        }
        return sMyIP;
    }

    public static void setMyIP(String sMyIP) {
        UserUtil.saveMyIp(sMyIP);
        ApplicationUtil.sMyIP = sMyIP;
    }

    public static UserBean getUserBean() {
        if (sUserBean == null){
            sUserBean = UserUtil.getUser();
        }
        return sUserBean;
    }

    public static void setUserBean(UserBean sUserBean) {
        ApplicationUtil.sUserBean = sUserBean;
    }

    public static Context getContxet() {
        return sContxet;
    }

    public static void setContxet(Context sContxet) {
        ApplicationUtil.sContxet = sContxet;
    }
}
