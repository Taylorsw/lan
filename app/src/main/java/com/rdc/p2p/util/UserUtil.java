package com.rdc.p2p.util;

import android.content.SharedPreferences;

import com.rdc.p2p.model.UserBean;

public class UserUtil {

    public static void saveMyIp(String ip){
        SharedPreferences.Editor editor = ApplicationUtil.getContxet().getSharedPreferences("user",0).edit();
        editor.putString("ip",ip);
        editor.apply();
    }

    public static String getMyIp(){
        SharedPreferences sp = ApplicationUtil.getContxet().getSharedPreferences("user",0);
        return sp.getString("ip","");
    }

    public static void saveUser(UserBean userBean){
        SharedPreferences.Editor editor = ApplicationUtil.getContxet().getSharedPreferences("user",0).edit();
        editor.putInt("imageId",userBean.getUserImageId());
        editor.putString("nickName",userBean.getNickName());
        editor.apply();
    }


    /**
     * 获取用户信息
     */
    public static UserBean getUser(){
        UserBean userBean = new UserBean();
        SharedPreferences sp = ApplicationUtil.getContxet().getSharedPreferences("user",0);
        userBean.setUserImageId(sp.getInt("imageId",0));
        userBean.setNickName(sp.getString("nickName",""));
        return userBean;
    }
}
